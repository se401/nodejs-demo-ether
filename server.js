const express = require('express');
var bodyParser = require('body-parser');

const Contracts = require('./app/contracts.js');
const accountService = require('./services/account');

const port = process.env.PORT || 3000;

const app = express();
app.use(bodyParser.json());

// Routes
// Get all unlocked accounts
app.get('/accounts', function (req, res) {
    Contracts.web3.eth.getAccounts(function (err, accounts) {
        if (err) { res.status(500).json({ error: err }); return; }
        res.json({
            accounts: accounts
        });
    });
});

// Get storage contract value
app.get('/storage', function (req, res) {
    Contracts.SimpleStorage.deployed().then(function (instance) {
        instance.get.call().then(function (value) {
            res.json({
                value: value
            });
        });
    });
});

// Set storage contract value
app.post('/storage', function (req, res) {
    var value = req.body.value;
    Contracts.SimpleStorage.deployed().then(function (instance) {
        instance.set(value, { from: Contracts.accounts[0] }).then(function (tx) {
            res.json({
                transaction: tx
            });
        });
    });
});


//account
app.post('/account/create', async function(req,res){
    let data = await accountService.createAccount(req,res);
    res.json({
        payload: data
    })
})

//
app.post("/account/get-balance",async function(req,res){
    let data = await accountService.getBalance(req,res);
    res.json({
        data
    })
})

app.post("/account/send-eth",async function(req,res){
    let transaction = await accountService.sendEth(req,res);
    res.json({
        transaction
    })
})

app.listen(port);