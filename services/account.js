const Web3 = require("web3");
const Contracts = require("../app/contracts.js");

account = {
  createAccount: async function (req, res) {
    // var provider = new Web3.providers.HttpProvider("http://127.0.0.1:7545");
    // var web3 = new Web3(provider);
    let password = req.body.password;
    let data = await Contracts.web3.eth.personal.newAccount(password);

    // let privateKey = data.getPrivateKeyString();
    // let address = data.getAddressString();
    return {
      data,
    };
  },
  getBalance: async function (req, resp) {
    let addr = req.body.address;
    let balance = await Contracts.web3.eth.getBalance(addr);
    balance = await Contracts.web3.utils.fromWei(balance, "ether");
    return {
      addr: addr,
      balance: balance,
    };
  },
  sendEth: async function (req, resp) {
    let srcAddr = req.body.fromAddr;
    let dstAddr = req.body.toAddr;
    let amount = req.body.amount;

    //let amoutStr = amount.toString();
    //convert to eth
    let cvt_amount = await Contracts.web3.utils.toWei(amount, "ether");

    let objTrans = {
      from: srcAddr,
      to: dstAddr,
      value: cvt_amount,
    };

    let transaction = await Contracts.web3.eth.sendTransaction(objTrans);
    return {
        transaction
    }
  },
};

module.exports = account;
